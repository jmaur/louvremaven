package tp.louvre.view;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import tp.louvre.main.Systeme;


 
public class SystemeVue extends JFrame implements Observer {
	// Ma fenetre
	private JPanel pan = new JPanel();
  
	// Mes boutons
	private JButton bouton = new JButton("Cr�er un formulaire");
	private JButton bouton2 = new JButton("Consulter vos formulaire");
	
	// Affichage text
	
	private JLabel label1 = new JLabel("Nombre de demandes: ");
	private int compteur;
	private JLabel affiche = new JLabel (""+compteur);
  
	//Objet � observer
	private Systeme syst;
 
	  public SystemeVue(Systeme s){
		  this.syst=s;
		  String text;
		//D�finit un titre pour notre fen�tre
	    this.setTitle("Mus� du Louvre - Offres"); 
	    //D�finit sa taille : 400 pixels de large et 100 pixels de haut
	    this.setSize(250, 150); 
	    //Termine le processus lorsqu'on clique sur la croix rouge
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    //Nous demandons maintenant � notre objet de se positionner au centre
	    this.setLocationRelativeTo(null);   
	   
	    //Ajout du bouton � notre content pane
	    //bouton.setBounds(0, 0, 100, 50);
	    
	    pan.add(label1);
	    pan.add(affiche);
	    
	    //pan.add(bouton);
	    
	    this.setContentPane(pan);
	    this.setVisible(true);
	    
	    
	
	    /* La vue observera le mod�le */
		 syst.addObserver(this);
		 
	    //Et enfin, la rendre visible        
	    this.setVisible(true);
	 }
	  
	  public void compteur(Systeme arg){
		if(arg instanceof Systeme)
		  this.compteur= +arg.getListForm().size();
			affiche.setText(""+this.compteur);
	  }
	  
	 public void update (Observable observe, Object arg) {
			System.out.println("L'observateur est averti de l'action : "+arg);
			
			compteur((Systeme)arg);
			System.out.println(this.compteur);
	 }

}
