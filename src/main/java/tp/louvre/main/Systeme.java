package tp.louvre.main;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Scanner;

import tp.louvre.model.Element;
import tp.louvre.model.Formulaire;
import tp.louvre.model.Historien;
import tp.louvre.model.User;

//
//
//  Generated by StarUML(tm) Java Add-In
//
//  @ Project : Untitled
//  @ File Name : Syst�me.java
//  @ Date : 07/10/2013
//  @ Author : 
//
//




public class Systeme  extends Observable {
	private ArrayList<User> user;
	private ArrayList<Formulaire> listForm;
	private Element element;

	
	public Systeme()
	{
		user = new ArrayList<User>();
		listForm = new ArrayList<Formulaire>();
	}
	
	
	public void creerFormulaire(Historien hist, Formulaire form){
		
		form.remplirForm(hist);
		int iduser = this.getUser().indexOf(hist);
		((Historien) this.getUser().get(iduser)).addForm(form);
		this.getListForm().add(form);
		setChanged();
		notifyObservers(this);
	}
	
	
	
	// ---------------- Getter & Setter -----------------------//

	public ArrayList<Formulaire> getListForm() {
		return listForm;
	}

	public void setListForm(ArrayList<Formulaire> listForm) {
		this.listForm = listForm;
	}

	

	public ArrayList<User> getUser() {
		return user;
	}

	public void setUser(ArrayList<User> user) {
		this.user = user;
	}



	public Element getElement() {
		return element;
	}

	public void setElement(Element element) {
		this.element = element;
	}		
	
	
	
}
