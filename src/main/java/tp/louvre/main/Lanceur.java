package tp.louvre.main;

import java.util.Scanner;

import tp.louvre.model.Formulaire;
import tp.louvre.model.Historien;
import tp.louvre.model.User;
import tp.louvre.view.SystemeVue;




public class Lanceur {

	
	public static void main(String args[])
	{
		boolean saisie = false,run = true;
		int id = 1;
		int iduser = 0;
		String temp;
		
		Systeme sys = new Systeme(); // Cr�ation du syst�me
		SystemeVue vue = new SystemeVue(sys); // Cr�ation de la vue du syst�me
		sys.addObserver(vue); // Assigner la vue au syst�me
		
		
		//- Instance utile seulement pour les tests -//
		Formulaire form = new Formulaire();
		User henry = new Historien("hland","Klos52","Henry Landiers");
		sys.getUser().add(henry);
		iduser = sys.getUser().indexOf(henry);
		System.out.println("Bonjour Mr.Landiers");
		//------------------------------------------//
		
		
		Scanner sc = new Scanner(System.in);
		
		while(run)
		{
			while(!saisie)
	    	{
				System.out.println("Voulez vous:\n1 - Cr�er un formulaire.\n2 - Consulter vos formulaires.");
				temp = sc.nextLine();
						
				switch(temp){
	    		
		    	case "1":
		    		if(henry instanceof Historien)
		    			sys.creerFormulaire((Historien) henry, form);
		    	
		    		id++;
		    		saisie = true;
		    		break;
		    		
		    	case "2":
		    		temp = ((Historien) sys.getUser().get(iduser)).afficheListForm();
		    		System.out.println(temp);
		    		saisie = true;
		    		break;
		    		
		    	default:
		    		System.out.println("Erreur de saisie");
		    		saisie = false;
		    		break;
				}
	    	}
			
			if(saisie)
				saisie = false;
			
			System.out.println("Voulez-vous effectuez une autre op�ration? (o/n)");
			temp = sc.nextLine();
			
			if(temp.equals("N") || temp.equals("n"))
			{
				run = false;
				System.out.println("Au revoir et � bient�t");
			}				
		}
		
		
	}
}
