package tp.louvre.etat;
import tp.louvre.model.Formulaire;
import tp.louvre.model.Photo;
import tp.louvre.model.Photographe;
import tp.louvre.model.User;


public class AttentePhotographe extends EtatForm {
	
	public AttentePhotographe(Formulaire f)
	{
		super(f);
	}
	
	public void remplirForm(User user)
	{
		if(user instanceof Photographe)
			form.setPhotographe((Photographe) user);
		form.setEtat(new PrisEnCharge(form));
	}
	
	public void ajoutPhoto(Photo p)
	{
		throw new IllegalArgumentException("On ne peut pas ajouter une photo dans cet �tat");
	}
	
	public void validation()
	{}
	
	public String getEtatName()
	{
		return "Attente d'un photographe";
	}
	
	public String toString()
	{
		String temp = "Formulaire: "+form.getIdForm()+"\nCree le: "+form.getDateCreation().toString()+"\nEtat: "+this.getEtatName()+"\nDescription photographie: "+form.getDescription()+"\nOeuvre concern�e: "+form.getEl().getNomElement()+"\nDemand� par:"+form.getHist().getNom(); 
		return temp;
	}
}
