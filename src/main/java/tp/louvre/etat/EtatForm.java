package tp.louvre.etat;
import tp.louvre.model.Formulaire;
import tp.louvre.model.Photo;
import tp.louvre.model.User;


public abstract class EtatForm {
	
	protected Formulaire form;
	
	public EtatForm(Formulaire form)
	{
		this.form = form;
	}
	
	/**
	 * Permet de remplir le formulaire
	 * @param user
	 */
	public abstract void remplirForm(User user);
	public abstract void ajoutPhoto(Photo p);
	public abstract String getEtatName();
	public abstract void validation();
	public abstract String toString();
}
